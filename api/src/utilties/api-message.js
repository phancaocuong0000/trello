export const apiMessage = {
  registerSuccess: "Register Success! Please activate your email to start.",
  loginSuccess: "Login success!",
  sendMailForgotPassword: "Re-send the password, please check your email.",
  changePasswordSuccess: "Password successfully changed!",
  updateUserSuccess: "Update Success!",
};
